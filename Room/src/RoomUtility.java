import java.util.ArrayList;
import java.util.Scanner;

/**
 * The room utility class holds functionality for searching for a room and
 * providing summary statistics.
 *
 * @author Ashley Patterson
 * @since 20150327
 */
public class RoomUtility {

    /**
     * Room summary method shows the count of each type of room and the rooms
     * with most seats.
     *
     * @author Ashley Patterson
     * @since 20150327
     *
     * @param rooms
     */
    public static void roomSummary(ArrayList<Room> rooms) {
        int roomCount = 0;
        int compRoomCount = 0;
        int bioRoomCount = 0;
        int boardRoomCount = 0;
        int specialBoardRoomCount = 0;
        int largestNumberOfSeats = 0;
        String largestRoom = "";

        //if a room in the arraylist is an instance of a child class room, increments count for
        //that class; if not, increments parent class room.  Tracks largest room based on number of seats.
        for (Room currentRoom : rooms) {
            if (currentRoom instanceof ComputerRoom) {
                compRoomCount++;
            } else if (currentRoom instanceof BiologyLab) {
                bioRoomCount++;
            } else if (currentRoom instanceof BoardRoom) {
                if (currentRoom instanceof SpecialBoardRoom) {
                    specialBoardRoomCount++;
                } else {
                    boardRoomCount++;
                }
            } else {
                roomCount++;
            }
            
            /*
              Have to handle the case where there are two rooms with the same
              number of seats.
            */
            
            if (currentRoom.getNumberOfSeats() > largestNumberOfSeats) {
                largestNumberOfSeats = currentRoom.getNumberOfSeats();
                largestRoom = "" + currentRoom.getRoomNumber();
            } else if (currentRoom.getNumberOfSeats() == largestNumberOfSeats) {
                largestRoom += ", " + currentRoom.getRoomNumber();
            }
        }
        //displays summary of room count
        String summary = "Room Count Details Report\n"
                + "Room: " + roomCount
                + "\nComputer Room: " + compRoomCount
                + "\nBiology Lab: " + bioRoomCount
                + "\nBoard Room: " + boardRoomCount
                + "\nSpecial Board Room: " + specialBoardRoomCount
                + "\n\nLargest Room: Room#" + largestRoom + " with " + largestNumberOfSeats + " seats";
        System.out.println(summary);
    }

    /**
     * Find room method searches for existing rooms based on room type and
     * number of seats.
     *
     * @author Ashley Patterson
     * @since 20150327
     *
     * @param rooms
     */
    public static void findRoom(ArrayList<Room> rooms) {
        Scanner input = new Scanner(System.in);
        System.out.println("What type of room are you looking for?\n" + "1) Any Room\n"
                + "2) Computer Lab\n"
                + "3) Board Room\n"
                + "4) Special Board Room\n"
                + "5) Biology Lab\n");
        String roomType = input.nextLine();
        System.out.println("How many seats do you need?");
        int numberOfSeats = Integer.parseInt(input.nextLine());

        //case system for types of rooms; loops through rooms arraylist and returns matches based on
        //search criteria.
        switch (roomType) {
            case "1":
                for (Room desiredRoom : rooms) {
                    //at least the number of seats.
                    if (numberOfSeats <= desiredRoom.getNumberOfSeats()) {
                        String roomMatch = desiredRoom.toString();
                        System.out.println(roomMatch);
                    }
                }
                break;
            case "2":
                for (Room desiredRoom : rooms) {
                    if (desiredRoom instanceof ComputerRoom) {
                        if (numberOfSeats <= desiredRoom.getNumberOfSeats()) {
                            String roomMatch = desiredRoom.toString();
                            System.out.println(roomMatch);
                        }
                    }
                }
                break;
            case "3":
                for (Room desiredRoom : rooms) {
                    if (desiredRoom instanceof BoardRoom) {
                        if (numberOfSeats <= desiredRoom.getNumberOfSeats()) {
                            String roomMatch = desiredRoom.toString();
                            System.out.println(roomMatch);
                        }
                    }
                }
                break;
            case "4":
                for (Room desiredRoom : rooms) {
                    if (desiredRoom instanceof SpecialBoardRoom) {
                        if (numberOfSeats <= desiredRoom.getNumberOfSeats()) {
                            String roomMatch = desiredRoom.toString();
                            System.out.println(roomMatch);
                        }
                    }
                }
                break;
            case "5":
                for (Room desiredRoom : rooms) {
                    if (desiredRoom instanceof BiologyLab) {
                        if (numberOfSeats <= desiredRoom.getNumberOfSeats()) {
                            String roomMatch = desiredRoom.toString();
                            System.out.println(roomMatch);
                        }
                    }
                }
                break;
        }
    }
}
