import java.util.Scanner;

/**
 * This class holds the information and functionality related to a board room.
 *
 * @author BJ MacLean
 * @since 20150325
 */
public class BoardRoom extends Room {

    private boolean hasTeleconferencing;

    public BoardRoom(int roomNumber) {
        super(roomNumber);
    }

    public boolean isHasTeleconferencing() {
        return hasTeleconferencing;
    }

    public void setHasTeleconferencing(boolean hasTeleconferencing) {
        this.hasTeleconferencing = hasTeleconferencing;
    }

    @Override
    public String toString() {
        return super.toString() + "\nHas teleconferencing: " + hasTeleconferencing;
    }

    /**
     * Get the details from the user about this class. This will invoke the
     * super method to get the base class attributes.
     *
     * @author BJ MacLean
     * @since 20150325
     */
    public void getRoomDetailsFromUser() {
        super.getRoomDetailsFromUser();
        Scanner input = new Scanner(System.in);
        System.out.print("Does this room have teleconferencing? (Y/N):");
        hasTeleconferencing = input.nextLine().equalsIgnoreCase("y");
        ;
    }
}
